import "./App.css";
import { BrowserRouter as Router, Routes, Route, Link } from "react-router-dom";
import ElencoComponent from "./components/ElencoComponent";
import InserimentoComponent from "./components/InserimentoComponent";
import DettaglioMiddleComponent from "./components/DettaglioMiddleComponent";
import ResetVotiComponent from "./components/ResetVotiComponent";

function App() {
  return (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <Link className="nav-link" to="/">
          Gestione Locali
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav">
            <li className="nav-item">
              <Link className="nav-link" to="/admin/aggiungi">
                Aggiungi Locale
              </Link>
            </li>
          </ul>
        </div>
        <ResetVotiComponent />
      </nav>

      <div className="container mt-2">
        <Routes>
          <Route path="/" element={<ElencoComponent />} />
          <Route path="/admin/aggiungi" element={<InserimentoComponent />} />
          <Route
            path="/admin/dettaglio/:id"
            element={<DettaglioMiddleComponent />}
          />
        </Routes>
      </div>
    </Router>
  );
}

export default App;

/* 
TODO
Aggiungere Schermate ERRORE
Validazione INPUT FORM
CONTROLLARE REDIRECT DELLE AZIONI DI AGGIUNTA E VOTO
2. CHALLENGE - RESET VOTO LOCALE (fatelo alla fine!)  (AGGIUNGERE MODALE O MESSAGGIO WARNING)
3. CHALLENGE - Possibilità di filtrare tra i locali per Città/Provincia/Regione
*/
