import axios from "axios";
import React, { Component } from "react";
import VotoComponent from "./VotoComponent";

export default class DettaglioLocaleComponent extends Component {
  urlDettaglio = "http://127.0.0.1:4000/locali/";
  constructor(props) {
    super(props);

    const { identificatore } = this.props;

    this.state = {
      identificatore,
      locale: {
        nome: null,
        descrizione: null,
        citta: null,
        provincia: null,
        regione: null,
        latitudine: null,
        longitudine: null,
        voto: null,
      },
      showModifica: false,
      modificaSuccess: false,
    };
  }

  componentDidMount() {
    axios
      .get(this.urlDettaglio + this.state.identificatore)
      .then((risultato) => {
        let arrayRis = risultato.data;
        if (arrayRis.length > 0) {
          this.setState((stato) => ({
            locale: arrayRis[0],
          }));
        } else {
          console.log("errore non trovato");
        }
      });
  }

  eliminaLocale = () => {
    axios
      .delete(this.urlDettaglio + this.state.identificatore)
      .then((risultato) => {
        document.location.href = "/";
      })
      .catch((errore) => {
        console.log(errore);
      });
  };

  modificaLocale = (evt) => {
    evt.preventDefault();
    let localeTemp = {
      nome: evt.target.inputNome.value,
      descrizione: evt.target.inputDescrizione.value,
      citta: evt.target.inputCitta.value,
      provincia: evt.target.inputProvincia.value,
      regione: evt.target.inputRegione.value,
      latitudine: evt.target.inputLatitudine.value,
      longitudine: evt.target.inputLongitudine.value,
      voto: this.state.locale.voto,
    };
    axios
      .put(this.urlDettaglio + this.state.identificatore, localeTemp)
      .then((risultato) => {
        this.setState((stato) => ({ modificaSuccess: true }));
        setTimeout(() => {
          document.location.reload();
        }, 1500);
      })
      .catch((errore) => {
        console.log(errore);
      });
  };
  abilitaModifica = () => {
    this.setState((stato) => ({
      showModifica: !stato.showModifica,
    }));
  };

  render() {
    let modificaSuccess = "";
    if (this.state.modificaSuccess) {
      modificaSuccess = (
        <div className="alert alert-success mt-3 text-center" role="alert">
          Modifica andata a buon fine
        </div>
      );
    }

    const cardDettaglio = (
      <div className="col">
        <div className="card">
          <div className="card-body">
            <h5 className="card-title">{this.state.locale.nome}</h5>
            <p className="card-text">
              {this.state.locale.citta},&nbsp;{this.state.locale.provincia}
              ,&nbsp;
              {this.state.locale.regione}
            </p>
            <p className="card-text">
              {this.state.locale.latitudine},&nbsp;
              {this.state.locale.longitudine}
            </p>
            <h2 className="card-title">
              Voto:&nbsp;
              {this.state.locale.voto === null
                ? "Senza voto"
                : this.state.locale.voto}
            </h2>
          </div>
          <button
            className="btn btn-outline-danger"
            onClick={this.eliminaLocale}
          >
            Elimina
          </button>
          <button
            className="btn btn-outline-warning mt-1"
            onClick={this.abilitaModifica}
          >
            Modifica
          </button>
        </div>
        <VotoComponent
          id={this.state.identificatore}
          voto={this.state.locale.voto}
        />
      </div>
    );

    const formModifica = (
      <div>
        <form onSubmit={this.modificaLocale}>
          <div className="form-group">
            <label htmlFor="campoNome">Nome</label>
            <input
              type="text"
              name="inputNome"
              id="campoNome"
              className="form-control"
              defaultValue={this.state.locale.nome}
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoDescrizione">Descrizione</label>
            <input
              type="text"
              name="inputDescrizione"
              id="campoDescrizione"
              className="form-control"
              defaultValue={this.state.locale.descrizione}
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoCitta">Citta</label>
            <input
              type="text"
              name="inputCitta"
              id="campoCitta"
              className="form-control"
              defaultValue={this.state.locale.citta}
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoProvincia">Provincia</label>
            <input
              type="text"
              name="inputProvincia"
              id="campoProvincia"
              className="form-control"
              defaultValue={this.state.locale.provincia}
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoRegione">Regione</label>
            <input
              type="text"
              name="inputRegione"
              id="campoRegione"
              className="form-control"
              defaultValue={this.state.locale.regione}
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoLatitudine">Latitudine</label>
            <input
              type="text"
              name="inputLatitudine"
              id="campoLatitudine"
              className="form-control"
              defaultValue={this.state.locale.latitudine}
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoLongitudine">Longitudine</label>
            <input
              type="text"
              name="inputLongitudine"
              id="campoLongitudine"
              className="form-control"
              defaultValue={this.state.locale.longitudine}
            />
          </div>
          <button className="btn btn-outline-success w-100 mt-4">
            Modifica Locale
          </button>
          <button
            className="btn btn-outline-info w-100 mt-1"
            onClick={this.abilitaModifica}
          >
            Annulla
          </button>
        </form>
        {modificaSuccess && modificaSuccess}
      </div>
    );
    return <>{this.state.showModifica ? formModifica : cardDettaglio}</>;
  }
}
