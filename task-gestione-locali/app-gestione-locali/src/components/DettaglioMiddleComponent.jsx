import React from "react";
import { useParams } from "react-router-dom";
import DettaglioLocaleComponent from "./DettaglioLocaleComponent";

export default function DettaglioMiddleComponent() {
  let { id } = useParams();
  return (
    <>
      <DettaglioLocaleComponent identificatore={id} />
    </>
  );
}
