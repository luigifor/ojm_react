import React, { Component } from "react";

export class SearchBarComponent extends Component {
  urlRicerca = "http://127.0.0.1:4000/locali/";
  constructor(props) {
    super(props);

    this.state = {};
  }
  pulsanteRicerca = (evt) => {
    evt.preventDefault();

    const valore = evt.target.inputValore.value;
    const categoria = evt.target.inputCat.value;

    this.props.ricerca(categoria, valore);
  };
  pulsanteAnnulla = () => {
    this.props.annullaRicerca();
  };

  render() {
    return (
      <div>
        <form className="d-flex" onSubmit={this.pulsanteRicerca}>
          <input
            className="form-control"
            type="search"
            placeholder="Cerca"
            aria-label="Search"
            name="inputValore"
            id="campoValore"
          />

          <select className="form-control" name="inputCat" id="campoCat">
            <option value="citta">Citta</option>
            <option value="provincia">Provincia</option>
            <option value="regione">Regione</option>
          </select>

          <button className="btn btn-outline-success w-100">Cerca</button>
          <button
            type="button"
            className="btn btn-outline-secondary"
            onClick={this.pulsanteAnnulla}
          >
            x
          </button>
        </form>
      </div>
    );
  }
}

export default SearchBarComponent;
