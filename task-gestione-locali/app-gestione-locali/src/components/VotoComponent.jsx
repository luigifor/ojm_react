import React, { Component } from "react";
import axios from "axios";

export class VotoComponent extends Component {
  urlVoto = "http://127.0.0.1:4000/vota/";
  urlDettaglio = "http://127.0.0.1:4000/locali/";
  constructor(props) {
    super(props);
    const { id, voto } = this.props;
    this.state = {
      id,
      voto,
      showErrore: false,
    };
  }

  inserisciVoto = (evt) => {
    evt.preventDefault();
    if (this.state.voto === null) {
      let localeTemp = {
        voto: Number.parseInt(evt.target.inputVoto.value),
      };
      axios
        .put(this.urlVoto + this.state.id, localeTemp)
        .then((risultato) => {
          document.location.reload();
          //   console.log(risultato);
        })
        .catch((errore) => {
          console.log(errore);
        });
    } else {
      // console.log("Hai gia votato questo locale");
      this.setState((state) => ({ showErrore: true }));
      setTimeout(() => {
        this.setState((state) => ({ showErrore: false }));
      }, 2000);
    }
  };
  componentDidMount() {
    axios.get(this.urlDettaglio + this.state.id).then((risultato) => {
      let arrayRis = risultato.data;
      if (arrayRis.length > 0) {
        this.setState((stato) => ({
          voto: arrayRis[0].voto,
        }));
      } else {
        console.log("errore non trovato");
      }
    });
  }

  render() {
    let errore = "";
    if (this.state.showErrore) {
      errore = (
        <div className="alert alert-danger mt-3" role="alert">
          Hai gia votato questo locale!
        </div>
      );
    }

    return (
      <>
        <h2 className="text-center mt-4">Vota questo locale</h2>
        <form onSubmit={this.inserisciVoto}>
          <div className="form-group">
            <label htmlFor="campoVoto"></label>
            <select
              name="inputVoto"
              id="campoVoto"
              className="form-control mt-2"
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
            </select>
            <button className="btn btn-outline-success w-100 mt-2">Vota</button>
            {errore && errore}
          </div>
        </form>
      </>
    );
  }
}

export default VotoComponent;
