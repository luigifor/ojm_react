import axios from "axios";
import React, { Component } from "react";

export class ResetVotiComponent extends Component {
  urlReset = "http://127.0.0.1:4000/admin/reset";
  constructor(props) {
    super(props);

    this.state = {};
  }
  resetVoti = () => {
    axios
      .put(this.urlReset)
      .then((risultato) => {
        console.log("reset avvenuto con successp");
        document.location.reload();
      })
      .catch((errore) => {
        console.log("errore di reset");
      });
  };

  render() {
    return (
      <div>
        <button
          onClick={this.resetVoti}
          className="btn btn-danger"
          type="button"
        >
          Reset Voti
        </button>
      </div>
    );
  }
}

export default ResetVotiComponent;
