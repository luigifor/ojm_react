import React, { Component } from "react";
import axios from "axios";
import RigaComponent from "./RigaComponent";
import SearchBarComponent from "./SearchBarComponent";

export class ElencoComponent extends Component {
  urlRiferimento = "http://127.0.0.1:4000/locali/list";
  urlRicerca = "http://127.0.0.1:4000/locali/";
  constructor(props) {
    super(props);

    this.state = {
      elencoLocali: [],
    };
  }
  aggiornaElenco = () => {
    axios
      .get(this.urlRiferimento)
      .then((risultato) => {
        this.setState((stato) => ({ elencoLocali: risultato.data }));
      })
      .catch((errore) => {
        console.log(errore);
      });
  };

  componentDidMount() {
    this.aggiornaElenco();
    this.aggAuto = setInterval(() => {
      this.aggiornaElenco();
    }, 2000);
  }
  componentWillUnmount() {
    clearInterval(this.aggAuto);
  }

  funzioneRicerca = (categoria, valore) => {
    clearInterval(this.aggAuto);
    axios
      .get(this.urlRicerca + categoria + "/" + valore)
      .then((risultato) => {
        this.setState((stato) => ({ elencoLocali: risultato.data }));
        console.log(this.state.elencoLocali);
      })
      .catch((errore) => {
        console.log(errore);
      });
  };

  annullaRicerca = () => {
    this.aggiornaElenco();
    this.aggAuto = setInterval(() => {
      this.aggiornaElenco();
    }, 2000);
  };

  render() {
    return (
      <div className="row">
        <div className="col-md-8">
          <h1>Elenco</h1>
        </div>
        <div className="col-md-4">
          <SearchBarComponent
            ricerca={this.funzioneRicerca}
            annullaRicerca={this.annullaRicerca}
          />
        </div>

        <table className="table table-striped">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Descrizione</th>
              <th>Citta</th>
              <th>Provincia</th>
              <th>Regione</th>
              <th>Latitudine</th>
              <th>Longitudine</th>
              <th>Voto</th>
            </tr>
          </thead>
          <tbody>
            {this.state.elencoLocali.map((locale, idx) => (
              <RigaComponent key={idx} locale={locale} />
            ))}
            {/* <ul>
              {this.state.elencoLocali.map((locale, idx) => (
                <li>{locale.nome}</li>
              ))}
            </ul> */}
          </tbody>
        </table>
      </div>
    );
  }
}

export default ElencoComponent;
