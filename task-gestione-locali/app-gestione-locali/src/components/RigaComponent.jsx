import React, { Component } from "react";
import { Link } from "react-router-dom";

export class RigaComponent extends Component {
  constructor(props) {
    super(props);
    const { locale } = this.props;
    const {
      id,
      nome,
      descrizione,
      citta,
      provincia,
      regione,
      latitudine,
      longitudine,
      voto,
    } = locale;
    this.state = {
      id,
      nome,
      descrizione,
      citta,
      provincia,
      regione,
      latitudine,
      longitudine,
      voto,
    };
  }

  render() {
    // console.log(this.state);
    const urlLocale = "admin/dettaglio/" + this.state.id;
    return (
      <tr>
        <td>
          <Link to={urlLocale}>{this.state.nome}</Link>
        </td>
        <td>{this.state.descrizione}</td>
        <td>{this.state.citta}</td>
        <td>{this.state.provincia}</td>
        <td>{this.state.regione}</td>
        <td>{this.state.latitudine}</td>
        <td>{this.state.longitudine}</td>
        <td>{this.state.voto === null ? "" : this.state.voto}</td>
      </tr>
    );
  }
}

export default RigaComponent;
