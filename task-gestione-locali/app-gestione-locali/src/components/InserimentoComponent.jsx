import axios from "axios";
import React, { Component } from "react";

export class InserimentoComponent extends Component {
  urlInserimento = "http://127.0.0.1:4000/aggiungi";
  constructor(props) {
    super(props);

    this.state = {
      showSuccess: false,
    };
  }

  inserisciLocale = (evt) => {
    evt.preventDefault();

    let localeTemp = {
      nome: evt.target.inputNome.value,
      descrizione: evt.target.inputDescrizione.value,
      citta: evt.target.inputCitta.value,
      provincia: evt.target.inputProvincia.value,
      regione: evt.target.inputRegione.value,
      latitudine: evt.target.inputLatitudine.value,
      longitudine: evt.target.inputLongitudine.value,
      voto: null,
    };

    axios
      .post(this.urlInserimento, localeTemp)
      .then((risultato) => {
        console.log("sono nel then");
        console.log(risultato);
        if (risultato.data.status && risultato.data.status === "success") {
          console.log("inserimento andato a buon fine");
          this.setState((state) => ({ showSuccess: true }));

          setTimeout(() => {
            document.location.href = "/";
          }, 1500);
        }
      })
      .catch((errore) => {
        console.log(errore);
      });
  };

  render() {
    let insSuccess = "";
    if (this.state.showSuccess) {
      insSuccess = (
        <div className="alert alert-success mt-3 text-center" role="alert">
          Inserimento andato a buon fine
        </div>
      );
    }
    return (
      <div>
        <h1>INSERISCI</h1>
        <form onSubmit={this.inserisciLocale}>
          <div className="form-group">
            <label htmlFor="campoNome">Nome</label>
            <input
              type="text"
              name="inputNome"
              id="campoNome"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoDescrizione">Descrizione</label>
            <input
              type="text"
              name="inputDescrizione"
              id="campoDescrizione"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoCitta">Citta</label>
            <input
              type="text"
              name="inputCitta"
              id="campoCitta"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoProvincia">Provincia</label>
            <input
              type="text"
              name="inputProvincia"
              id="campoProvincia"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoRegione">Regione</label>
            <input
              type="text"
              name="inputRegione"
              id="campoRegione"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoLatitudine">Latitudine</label>
            <input
              type="text"
              name="inputLatitudine"
              id="campoLatitudine"
              className="form-control"
            />
          </div>
          <div className="form-group">
            <label htmlFor="campoLongitudine">Longitudine</label>
            <input
              type="text"
              name="inputLongitudine"
              id="campoLongitudine"
              className="form-control"
            />
          </div>
          {/* <div className="form-group">
            <label htmlFor="campoVoto">Voto</label>
            <input
              type="text"
              name="inputVoto"
              id="campoVoto"
              className="form-control"
            />
          </div> */}
          <button className="btn btn-outline-success w-100 mt-4">
            Inserisci Locale
          </button>
        </form>
        {insSuccess && insSuccess}
      </div>
    );
  }
}

export default InserimentoComponent;
