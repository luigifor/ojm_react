const express = require("express");
const bodyParser = require("body-parser");
const pool = require("./dbConfig");
const cors = require("cors");
const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extender: true }));

const port = 4000;

app.listen(port, () => {
  console.log(`Sono in ascolto sulla porta ${port}`);
});

app.get("/locali/list", async (req, res) => {
  try {
    let queryStr = "SELECT * FROM locale";
    const lista = await pool.query(queryStr);
    res.json(lista.rows);
  } catch (error) {
    console.log(error);
  }
});

app.get("/locali/:id", async (req, res) => {
  try {
    let queryStr = `SELECT * FROM locale WHERE id=${req.params.id} `;
    const locale = await pool.query(queryStr);
    res.json(locale.rows);
  } catch (error) {
    console.log(error);
  }
});

app.get("/locali/:categoria/:valore", async (req, res) => {
  try {
    let queryFiltro = `SELECT * FROM locale WHERE ${req.params.categoria}='${req.params.valore}' `;
    const localiFiltrati = await pool.query(queryFiltro);
    res.json(localiFiltrati.rows);
  } catch (error) {
    console.log(error);
  }
});

app.post("/aggiungi", async (req, res) => {
  try {
    let queryIns =
      `insert into locale (nome,descrizione,citta,provincia,regione,latitudine,longitudine,voto) ` +
      `values ('${req.body.nome}','${req.body.descrizione}','${req.body.citta}','${req.body.provincia}',` +
      `'${req.body.regione}',${req.body.latitudine},${req.body.longitudine},${req.body.voto});`;

    const insLocale = await pool.query(queryIns);
    if (insLocale.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});

app.delete("/locali/:id", async (req, res) => {
  try {
    let queryDel = `DELETE FROM locale WHERE id=${req.params.id}`;

    const delLocale = await pool.query(queryDel);
    if (delLocale.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});

app.put("/locali/:id", async (req, res) => {
  try {
    let queryUpd =
      `UPDATE locale SET ` +
      `nome='${req.body.nome}',descrizione='${req.body.descrizione}',` +
      `citta='${req.body.citta}',provincia='${req.body.provincia}',regione='${req.body.regione}',` +
      `latitudine=${req.body.latitudine},longitudine=${req.body.longitudine},voto=${req.body.voto}` +
      `WHERE id=${req.params.id}`;

    const updLocale = await pool.query(queryUpd);

    if (updLocale.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});

app.put("/vota/:id", async (req, res) => {
  try {
    let queryUpdVoto =
      `UPDATE locale SET ` +
      `voto=${req.body.voto}` +
      `WHERE id=${req.params.id}`;

    const updVotoLocale = await pool.query(queryUpdVoto);

    if (updVotoLocale.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});

app.put("/admin/reset", async (req, res) => {
  try {
    let queryReset = `UPDATE locale SET voto=null`;

    const resetVoti = await pool.query(queryReset);

    if (resetVoti.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});
