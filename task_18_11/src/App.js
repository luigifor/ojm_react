import "./App.css";
import InputFormComponent from "./components/InputFormComponent";

function App() {
  return (
    <div>
      <InputFormComponent />
    </div>
  );
}

export default App;
