import React, { Component } from "react";

export class RigaComponent extends Component {
  render() {
    const { nome, cognome, sesso, note } = this.props;
    return (
      <>
        <tr>
          <td>{nome}</td>
          <td>{cognome}</td>
          <td>{sesso}</td>
          <td>{note}</td>
        </tr>
      </>
    );
  }
}

export default RigaComponent;
