import React, { Component } from 'react';
import RigaComponent from './RigaComponent';

export class InputFormComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nome: 'ND',
      cognome: 'ND',
      sesso: '',
      note: 'ND',
      dati: [],
    };
  }
  invioForm = evt => {
    evt.preventDefault();
    let datiArr = [...this.state.dati];
    datiArr.push({
      nome: evt.target.campoNome.value,
      cognome: evt.target.campoCognome.value,
      sesso: evt.target.campoSesso.value,
      note: evt.target.campoNote.value,
    });

    this.setState(state => ({
      nome: evt.target.campoNome.value,
      cognome: evt.target.campoCognome.value,
      sesso: evt.target.campoSesso.value,
      note: evt.target.campoNote.value,
      dati: datiArr,
    }));
  };

  render() {
    return (
      <div className="container-fluid">
        <h1>Form d'Inserimento</h1>

        <form onSubmit={this.invioForm} className="form-group">
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="campoNome">Nome</label>
                <input
                  type="text"
                  name="inputNome"
                  id="campoNome"
                  placeholder="Inserisci il valore..."
                  className="form-control"
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="campoCognome">Cognome</label>
                <input
                  type="text"
                  name="inputCognome"
                  id="campoCognome"
                  placeholder="Inserisci il valore..."
                  className="form-control"
                />
              </div>
            </div>
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="camposesso">Sesso</label>
                <select
                  name="inputSesso"
                  id="campoSesso"
                  className="form-control"
                >
                  <option value="M">Maschio</option>
                  <option value="F">Femmina</option>
                  <option value="A">Altro</option>
                </select>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label htmlFor="campoNote">Note</label>
                <textarea
                  name="inputNote"
                  id="campoNote"
                  cols="30"
                  rows="10"
                  className="form-control"
                ></textarea>
              </div>
            </div>
          </div>

          <div className="row mt-2">
            <div className="col-md-4"></div>
            <div className="col-md-4">
              <button className="btn btn-primary btn-block">Inserisci</button>
            </div>
            <div className="col-md-4"></div>
          </div>
        </form>

        <hr />

        <h1 className="mt-4">Dettagli</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Nome</th>
              <th>Cognome</th>
              <th>Sesso</th>
              <th>Note</th>
            </tr>
          </thead>
          <tbody>
            {this.state.dati.map((x, i) => (
              <RigaComponent
                key={i}
                nome={x.nome}
                cognome={x.cognome}
                note={x.note}
                sesso={x.sesso}
              />
            ))}
          </tbody>
        </table>
      </div>
    );
  }
}

export default InputFormComponent;
