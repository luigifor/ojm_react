import React, { Component } from 'react'
import ElementoRicettaComponent from './ElementoRicettaComponent'

export class ElencoRicetteComponent extends Component {
    constructor(props) {
        super(props)

    }
    render() {
        const { titolo, ing } = this.props
        return (
            <React.Fragment>
                <table>
                    <thead>
                        <tr>
                            <th>{titolo}</th>
                        </tr>
                    </thead>
                    <tbody>
                        {ing.map((obj, id) => <ElementoRicettaComponent key={id} nome={obj.nome} descrizione={obj.desc} quantita={obj.qnty} unita={obj.unit} />)}
                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default ElencoRicetteComponent
