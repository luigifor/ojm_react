import React, { Component } from 'react'
import ElencoRicetteComponent from './ElencoRicetteComponent'
export class ElencoTuttoComponent extends Component {
    constructor(props) {
        super(props)
        this.ricette = [
            {
                titolo: "Pasta",
                ing: [
                    {
                        nome: "Pasta",
                        desc: "Pasta artigianale",
                        qnty: "500",
                        unit: "g"
                    },
                    {
                        nome: "Olio",
                        desc: "Olio EVO",
                        qnty: "0.2",
                        unit: "l"
                    }
                ]
            },
            {
                titolo: "Tonno e Fagioli",
                ing: [
                    {
                        nome: "Tonno",
                        desc: "Tonno pinna gialla",
                        qnty: "300",
                        unit: "g"
                    },
                    {
                        nome: "Fagioli",
                        desc: "Fagioli Cannellini",
                        qnty: "300",
                        unit: "g"
                    }
                ]
            },

        ]
    }
    render() {
        return (
            <React.Fragment>
                {(this.ricette.map((obj, idx) => <ElencoRicetteComponent key={idx} titolo={obj.titolo} ing={obj.ing} />))}
            </React.Fragment>
        )
    }
}
export default ElencoTuttoComponent
