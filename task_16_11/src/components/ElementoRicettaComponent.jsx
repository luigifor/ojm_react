import React, { Component } from 'react'

export class ElementoRicettaComponent extends Component {
    render() {
        const { nome, descrizione, quantita, unita } = this.props
        return (
            <tr>
                <td>{nome} - {descrizione} - {quantita} - {unita}</td>
            </tr>
        )
    }
}

export default ElementoRicettaComponent
