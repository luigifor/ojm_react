
import './App.css';

import ElencoTuttoComponent from './components/ElencoTuttoComponent';

function App() {
  return (
    <div>
      <ElencoTuttoComponent />
    </div>
  );
}

export default App;
