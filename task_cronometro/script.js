let secCount = 0;
let minCount = 59;
let oraCount = 0;
let dayCount = 0;
let secStr = "";
let minStr = "";
let oraStr = "";

let cronometro = setInterval(() => {
  secStr = secCount < 10 ? `0${secCount}` : `${secCount}`;
  minStr = minCount < 10 ? `0${minCount}` : `${minCount}`;
  oraStr = oraCount < 10 ? `0${oraCount}` : `${oraCount}`;
  let outputStr = oraStr + ":" + minStr + ":" + secStr;
  console.log(outputStr);
  secCount++;
  if (secCount == 60) {
    secCount = 0;
    minCount++;
  }
  if (minCount == 60) {
    minCount = 0;
    oraCount++;
  }
  if (oraCount == 24) {
    oraCount = 0;
    dayCount++;
  }
}, 1000);
