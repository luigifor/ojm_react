import logo from './logo.svg';
import './App.css';
import ElencoGiocatoriComponent from './components/ElencoGiocatoriComponent'
import React from 'react';


function App() {
  return (
    <React.Fragment>

      <ElencoGiocatoriComponent />
    </React.Fragment>
  );
}

export default App;
