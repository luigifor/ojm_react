import React, { Component } from 'react'

export class ElementoGiocatoreComponent extends Component {

    render() {
        const { nome, cognome } = this.props
        return (
            <React.Fragment>
                <tr>

                    <td>{nome}</td>
                    <td>{cognome}</td>
                </tr>
            </React.Fragment>
        )
    }
}

export default ElementoGiocatoreComponent
