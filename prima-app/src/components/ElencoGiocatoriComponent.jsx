import React, { Component } from 'react'
import ElementoGiocatoreComponent from './ElementoGiocatoreComponent'


export class ElencoGiocatoriComponent extends Component {
    constructor(props) {
        super(props)

        this.giocatori = [
            {
                nome: "Lorenzo",
                cognome: "Insigne"
            },
            {
                nome: "Federico",
                cognome: "Chiesa"
            },
            {
                nome: "Emerson",
                cognome: "Palmieri"
            }
        ]
    }
    render() {
        return (
            <React.Fragment>
                <table>
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Cognome</th>
                        </tr>
                    </thead>
                    <tbody>

                        {this.giocatori.map((x, idx) => <ElementoGiocatoreComponent key={idx} nome={x.nome} cognome={x.cognome} />)}

                    </tbody>
                </table>
            </React.Fragment>
        )
    }
}

export default ElencoGiocatoriComponent
