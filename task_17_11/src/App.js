import "./App.css";
import PartitaComponent from "./components/PartitaComponent";

function App() {
  return (
    <div>
      <PartitaComponent squadraUno={"Squadra Uno"} squadraDue={"Squadra Due"} />
    </div>
  );
}

export default App;
