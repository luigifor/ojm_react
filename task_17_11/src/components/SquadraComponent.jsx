import React, { Component } from "react";

export class SquadraComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      punteggio: this.props.punteggio,
      nome: this.props.nome,
    };
  }

  aumentaPunteggio = () => {
    this.setState((state) => ({
      punteggio: Number.parseInt(state.punteggio) + 1,
    }));
  };

  diminuisciPunteggio = () => {
    this.setState((state) => ({
      punteggio: Number.parseInt(state.punteggio) - 1,
    }));
  };

  render() {
    return (
      <div className="col text-center">
        <h1>{this.state.nome}</h1>
        <h1>{this.state.punteggio}</h1>

        <button
          type="button"
          className="btn btn-outline-success"
          onClick={this.aumentaPunteggio}
        >
          Aumenta
        </button>
        <button
          type="button"
          className="btn btn-outline-danger"
          onClick={this.diminuisciPunteggio}
        >
          Diminuisci
        </button>
      </div>
    );
  }
}

export default SquadraComponent;
