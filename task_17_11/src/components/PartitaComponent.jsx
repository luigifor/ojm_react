import React, { Component } from "react";
import SquadraComponent from "./SquadraComponent";

export class PartitaComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      nomeSquadraUno: this.props.squadraUno,
      nomeSquadraDue: this.props.squadraDue,
      // punteggioUno: this.props.punteggioUno,
      // punteggioDue: this.props.punteggioDue,
    };
  }

  render() {
    return (
      <div className="row bg-light">
        <SquadraComponent punteggio={0} nome={this.state.nomeSquadraUno} />
        <div className="col">
          {/* <h1>
            {this.state.punteggioUno} - {this.state.punteggioDue}
          </h1> */}
        </div>
        <SquadraComponent punteggio={0} nome={this.state.nomeSquadraDue} />
      </div>
    );
  }
}

export default PartitaComponent;
