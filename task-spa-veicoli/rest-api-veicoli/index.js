const express = require("express");
const bodyParser = require("body-parser");
const pool = require("./dbConfig");
const cors = require("cors");

const app = express();

app.use(bodyParser.json());
app.use(cors());
app.use(bodyParser.urlencoded({ extender: true }));

const port = 4000;

app.listen(port, () => {
  console.log(`Sono in ascolto sulla porta ${port}`);
});

// getId
app.get("/get/:cf", async (req, res) => {
  try {
    let queryGetId = `SELECT id FROM utente WHERE cf='${req.params.cf}'`;
    const getId = await pool.query(queryGetId);
    res.json(getId.rows);
  } catch (error) {
    console.log(error);
  }
});

// lista veicoli
app.get("/list/:id", async (req, res) => {
  try {
    let queryList = `SELECT * FROM veicolo WHERE utente_id=${req.params.id}`;
    const lista = await pool.query(queryList);
    res.json(lista.rows);
  } catch (error) {
    console.log(error);
  }
});

// inserimento utente
app.post("/utente/registra", async (req, res) => {
  try {
    let queryReg = `INSERT INTO public.utente
        (nome, cognome, cf, indirizzo)
        VALUES('${req.body.nome}', '${req.body.cognome}', '${req.body.cf}', '${req.body.indirizzo}');`;

    const regUtente = await pool.query(queryReg);

    if (regUtente.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});

// inserimento veicoli

app.post("/inserisci/:id", async (req, res) => {
  try {
    let queryInsVeicolo = `INSERT INTO public.veicolo
        (targa, data_acquisto, marca, modello, utente_id)
        VALUES('${req.body.targa}', '${req.body.data}', '${req.body.marca}', '${req.body.modello}', ${req.params.id});`;

    const inserimentoVeicolo = await pool.query(queryInsVeicolo);
    if (inserimentoVeicolo.rowCount > 0) {
      res.json({ status: "success" });
    } else {
      res.json({ status: "error" });
    }
  } catch (error) {
    console.log(error);
  }
});
