const pg = require("pg");
const Pool = pg.Pool;

const pool = new Pool({
  user: "admin",
  password: "admin",
  database: "spa_veicoli",
  host: "localhost",
  port: 5432,
});

module.exports = pool;
