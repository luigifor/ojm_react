import React, { useState } from "react";

import { Button, Form, Row, Col } from "react-bootstrap";
import axios from "axios";

export default function FormComponent() {
  const urlRegistrazione = "http://127.0.0.1:4000/utente/registra";
  const urlInsVeicolo = "http://127.0.0.1:4000/inserisci/";
  const urlGetId = "http://127.0.0.1:4000/get/";

  const [ins, setIns] = useState(false);

  const [user, setUser] = useState({
    nome: "",
    cognome: "",
    cf: "",
    indirizzo: "",
  });

  const [veicolo, setVeicolo] = useState({
    targa: "",
    data: "",
    marca: "",
    modello: "",
  });

  const [id, setId] = useState(0);

  const annullaInserimento = () => {
    setIns(false);
  };

  const onChangeUserHandler = (evt) => {
    let value = evt.target.name;
    switch (value) {
      case "inputNome":
        setUser((prev) => ({
          ...prev,
          nome: evt.target.form.nome.value,
        }));
        break;
      case "inputCognome":
        setUser((prev) => ({
          ...prev,
          cognome: evt.target.form.cognome.value,
        }));
        break;
      case "inputCf":
        setUser((prev) => ({
          ...prev,
          cf: evt.target.form.cf.value,
        }));
        break;
      case "inputIndirizzo":
        setUser((prev) => ({
          ...prev,
          indirizzo: evt.target.form.indirizzo.value,
        }));
        break;

      default:
        break;
    }
  };

  const onChangeVeicoloHandler = (evt) => {
    let valueVeicolo = evt.target.name;

    switch (valueVeicolo) {
      case "inputTarga":
        setVeicolo((prev) => ({
          ...prev,
          targa: evt.target.form.targa.value,
        }));
        break;
      case "inputData":
        setVeicolo((prev) => ({
          ...prev,
          data: evt.target.form.data.value,
        }));
        break;
      case "inputMarca":
        setVeicolo((prev) => ({
          ...prev,
          marca: evt.target.form.marca.value,
        }));
        break;
      case "inputModello":
        setVeicolo((prev) => ({
          ...prev,
          modello: evt.target.form.modello.value,
        }));
        break;

      default:
        break;
    }
  };

  const inserisciUtente = () => {
    axios
      .post(urlRegistrazione, user)
      .then((risultato) => {
        console.log(risultato.data);
        setIns(true);
        getId();
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getId = () => {
    axios
      .get(urlGetId + user.cf)
      .then((ris) => {
        setId(ris.data[0].id);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  const inserisciVeicolo = () => {
    console.log(id);
    console.log(veicolo);
    axios
      .post(urlInsVeicolo + id, veicolo)
      .then((ris) => {
        console.log(ris.data);
      })
      .catch((err) => {
        console.log(err);
      });
  };

  const formInserimentoUtente = (
    <Form>
      <h1>Inserimento</h1>
      <Form.Group className="mt-2" controlId="nome">
        <Form.Label>Nome</Form.Label>
        <Form.Control
          type="text"
          name="inputNome"
          onChange={onChangeUserHandler}
        />
      </Form.Group>
      <Form.Group className="mt-2" controlId="cognome">
        <Form.Label>Cognome</Form.Label>
        <Form.Control
          type="text"
          name="inputCognome"
          onChange={onChangeUserHandler}
        />
      </Form.Group>
      <Form.Group className="mt-2" controlId="cf">
        <Form.Label>Codice Fiscale</Form.Label>
        <Form.Control
          type="text"
          name="inputCf"
          onChange={onChangeUserHandler}
        />
      </Form.Group>
      <Form.Group className="mt-2" controlId="indirizzo">
        <Form.Label>Indirizzo</Form.Label>
        <Form.Control
          type="text"
          name="inputIndirizzo"
          onChange={onChangeUserHandler}
        />
      </Form.Group>
      <div className="d-grid gap-2 mt-4">
        <Button variant="success" type="button" onClick={inserisciUtente}>
          Inserisci Utente
        </Button>
      </div>
    </Form>
  );
  const formInserimentoVeicolo = (
    <>
      <h3>
        Benvenuto, {user.nome} {id}
      </h3>

      <Form>
        <Form.Group className="mt-2" controlId="targa">
          <Form.Label>Targa</Form.Label>
          <Form.Control
            type="text"
            name="inputTarga"
            onChange={onChangeVeicoloHandler}
          />
        </Form.Group>
        <Form.Group className="mt-2" controlId="data">
          <Form.Label>Data</Form.Label>
          <Form.Control
            type="date"
            name="inputData"
            onChange={onChangeVeicoloHandler}
          />
        </Form.Group>
        <Form.Group className="mt-2" controlId="marca">
          <Form.Label>Marca</Form.Label>
          <Form.Control
            type="text"
            name="inputMarca"
            onChange={onChangeVeicoloHandler}
          />
        </Form.Group>
        <Form.Group className="mt-2" controlId="modello">
          <Form.Label>Modello</Form.Label>
          <Form.Control
            type="text"
            name="inputModello"
            onChange={onChangeVeicoloHandler}
          />
        </Form.Group>
        <div className="d-grid gap-2 mt-4">
          <Button variant="success" type="button" onClick={inserisciVeicolo}>
            Inserisci Veicolo
          </Button>
          <Button variant="danger" onClick={annullaInserimento}>
            Termina Inserimento
          </Button>
        </div>
      </Form>
    </>
  );

  return (
    <>
      <Row>
        <Col>{ins ? formInserimentoVeicolo : formInserimentoUtente}</Col>
      </Row>
    </>
  );
}
